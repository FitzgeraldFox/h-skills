package main

import (
	"fmt"
	"sync"
)

const (
	SetItemNotFoundError = "value by key not found"
)

type Set struct {
	mu sync.RWMutex

	m map[string]interface{}
}

func NewSet() *Set {
	return &Set{
		m: make(map[string]interface{}),
	}
}

func (s *Set) Get(key string) (interface{}, bool) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	if v, ok := s.m[key]; ok {
		return v, true
	}

	return nil, false
}

func (s *Set) Set(key string, item interface{}) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.m[key] = item
}

func (s *Set) Remove(key string) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if _, ok := s.m[key]; !ok {
		return fmt.Errorf(SetItemNotFoundError)
	}
	
	delete(s.m, key)

	return nil
}

func main() {
	type T struct {
		a string
		b int
	}

	wg := sync.WaitGroup{}

	count := 1000
	wg.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			defer wg.Done()
			s := NewSet()
			s.Set("1", 1)
			s.Set("2", "asd")
			s.Set("3", -4)
			s.Set("4", T{"a", 1})
			s.Set("5", T{"b", 2})
			s.Set("5", T{"b", 2})
			s.Set("6", T{"b", 3})

			fmt.Println("5:")
			item, exists := s.Get("5")
			if !exists {
				fmt.Println("5 not exists!")
			} else {
				fmt.Printf("5: %s\n", item)
			}

			s.Set("7", []int{1,2})
			s.Set("8", []string{"sad", "qwe"})
			s.Set("9", map[string]string{"asd": "qwe"})
			s.Set("9", "qwe")
			s.Set("2", 1)

			item, exists = s.Get("2")
			if !exists {
				fmt.Println("2 not exists!")
			} else {
				fmt.Printf("2: %s\n", item)
			}

			s.Set("3", "qwe")
			s.Set("10", "asd")

			fmt.Println("Unknown element 23123: ")

			item, exists = s.Get("23123")
			if !exists {
				fmt.Println("23123 not exists!")
			} else {
				fmt.Printf("23123: %s\n", item)
			}

			fmt.Println(s.m)
		}()
	}

	wg.Wait()
}
