package main

import (
	"fmt"
	"sync"
	"time"
)

type (
	key   int
	value string
)

type LRUItem struct {
	v value
	t time.Time
}

type LRU struct {
	mu *sync.RWMutex

	lruLen  int
	storage map[key]*LRUItem
}

func NewLRU(lruLen int) *LRU {
	return &LRU{
		mu:      &sync.RWMutex{},
		lruLen:  lruLen,
		storage: make(map[key]*LRUItem, lruLen),
	}
}

func (l *LRU) Add(elemKey key, elem value) {
	if (len(l.storage) + 1) > l.lruLen {
		// Если кэш заполнен, то удаляем элемент, к которому обращались дольше всего
		type elemToDelete struct {
			elem    LRUItem
			elemKey key
		}

		l.mu.RLock()
		delEl := elemToDelete{elem: LRUItem{t: time.Now()}}
		for k, elemInfo := range l.storage {
			if elemInfo.t.Before(delEl.elem.t) {
				delEl = elemToDelete{elem: *elemInfo, elemKey: k}
			}
		}
		l.mu.RUnlock()

		l.mu.Lock()
		if delEl.elemKey != 0 {
			delete(l.storage, delEl.elemKey)
		}
		l.mu.Unlock()
	}

	l.mu.Lock()
	l.storage[elemKey] = &LRUItem{v: elem, t: time.Now()}
	l.mu.Unlock()
}

func (l *LRU) Get(key key) (value, bool) {
	l.mu.Lock()
	defer l.mu.Unlock()

	if len(l.storage) == 0 {
		return "", false
	}

	if v, ok := l.storage[key]; ok {
		v.t = time.Now()

		return l.storage[key].v, true
	}


	return "", false
}

func (l *LRU) PrintAll() {
	l.mu.RLock()
	defer l.mu.RUnlock()

	fmt.Println("Printall:")

	for k, elemInfo := range l.storage {
		fmt.Printf("Key: %v, Value: %v, Time: %v\n", k, elemInfo.v, elemInfo.t)
	}
}

func main() {
	wg := sync.WaitGroup{}

	count := 100
	wg.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			defer wg.Done()
			lru := NewLRU(8)
			lru.Add(1, "a")
			time.Sleep(1 * time.Second)
			lru.Add(2, "b")
			time.Sleep(1 * time.Second)
			lru.Add(3, "c")
			time.Sleep(1 * time.Second)
			lru.Add(4, "d")
			time.Sleep(1 * time.Second)
			lru.Add(5, "e")
			time.Sleep(1 * time.Second)
			lru.Add(6, "f")
			time.Sleep(1 * time.Second)
			lru.Add(7, "g")
			time.Sleep(1 * time.Second)
			lru.Add(8, "h")
			time.Sleep(1 * time.Second)
			lru.Add(9, "i")
			time.Sleep(1 * time.Second)
			lru.Get(5)
			time.Sleep(1 * time.Second)
			lru.Get(6)
			time.Sleep(1 * time.Second)
			lru.Add(10, "j")
			time.Sleep(1 * time.Second)
			lru.Add(11, "k")
			lru.PrintAll()
			time.Sleep(1 * time.Second)
			lru.Get(2)
			time.Sleep(1 * time.Second)
			lru.Get(7)
			lru.Get(8)
			lru.PrintAll()
		}()
	}

	wg.Wait()
}
