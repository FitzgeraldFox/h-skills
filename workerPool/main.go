package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"os"
	"sort"
	"sync"
)

type File struct {
	Name string
	Body []byte
}

func main() {
	filesDir := flag.String("files-dir", "files", "Directory with files to sort (Required)")
	workerPoolCount := flag.Int("worker-pool", 4, "Worker pool count (Required)")
	needHelp := flag.String("help", "false", "Print all commands")
	flag.Parse()

	if *needHelp == "true" || *needHelp == "1" || *needHelp == "y" || *needHelp == "Y" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	filesDirPath := fmt.Sprintf("./%s", *filesDir)
	sortedFilesDirPath := fmt.Sprintf("./%s/sorted", *filesDir)

	files, err := os.ReadDir(filesDirPath)
	if err != nil {
		fmt.Println(fmt.Errorf("Ошибка чтения директории %s: %w", *filesDir, err).Error())

		os.Exit(1)
	}

	wg := &sync.WaitGroup{}
	wg.Add(*workerPoolCount)

	quit := make(chan struct{})
	ch := make(chan File, *workerPoolCount)
	for i := 0; i < *workerPoolCount; i++ {
		go worker(wg, ch, quit, sortedFilesDirPath)
	}

	initializedFiles := len(files)
	wg2 := &sync.WaitGroup{}
	wg2.Add(len(files))
	for _, f := range files {
		f := f
		if f.Type().IsDir() {
			wg2.Done()
			initializedFiles--
			continue
		}
		sortedFilePath := sortedFilesDirPath + "/" + "sorted-" + f.Name()
		if _, err := os.Stat(sortedFilePath); errors.Is(err, os.ErrNotExist) {
			err = os.WriteFile(sortedFilePath, []byte{}, 0777)
			if err != nil {
				initializedFiles--
				fmt.Printf("Ошибка при создании отсортированного файла %s: %v\n", f.Name(), err)
				wg2.Done()
				continue
			}
		}

		go handleFile(f, filesDirPath, ch, wg2)
	}

	wg2.Wait()

	for i := 0; i < *workerPoolCount; i++ {
		quit <- struct{}{}
	}

	wg.Wait()

	fmt.Println("Все файлы отсортированы!")
}

func handleFile(f fs.DirEntry, filesDirPath string, ch chan File, wg *sync.WaitGroup) {
	fmt.Println("handleFile started")
	file, err := os.Open(fmt.Sprintf("%s/%s", filesDirPath, f.Name()))
	if err != nil {
		log.Fatalf(
			"Ошибка при открытии файла для чтения тела файла %s; filesDirPath: %s: %v\n",
			f.Name(),
			filesDirPath,
			err,
		)
	}
	defer func() {
		fmt.Println("handleFile stopped")
		wg.Done()
		if err = file.Close(); err != nil {
			log.Fatalf("Ошибка закрытия файла %s: %w\n", f.Name(), err)
		}
	}()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		ch <- File{Name: f.Name(), Body: scanner.Bytes()}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func worker(wg *sync.WaitGroup, c chan File, quit chan struct{}, sortedFilesDirPath string) {
	fmt.Println("worker started")
	defer func() {
		fmt.Println("worker stopped")
		wg.Done()
	}()

	for {
		select {
		case <-quit:
			return
		case f := <-c:
			a := string(f.Body[:])
			_ = a
			sortedBody := []rune(string(f.Body[:]))
			sort.Slice(sortedBody, func(i, j int) bool { return sortedBody[i] < sortedBody[j] })

			sortedFileName := fmt.Sprintf("sorted-%s", f.Name)

			file, err := os.OpenFile(
				fmt.Sprintf("%s/%s", sortedFilesDirPath, sortedFileName),
				os.O_APPEND|os.O_WRONLY,
				os.ModeAppend,
			)
			if err != nil {
				file.Close()
				log.Fatalf("Ошибка при открытии файла для чтения тела файла %s: %v\n", f.Name, err)
			}

			if _, err := file.WriteString(string(sortedBody) + "\n"); err != nil {
				fmt.Println(fmt.Errorf("Ошибка создания отсортированного файла %s: %w\n", f.Name, err).Error())
				file.Close()

				continue
			}

			file.Close()
		}
	}
}
