module background-scheduler

go 1.21.0

require github.com/a-menshenin/in-memory-scheduler v1.0.2

require (
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
)
