package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	ims "github.com/a-menshenin/in-memory-scheduler"
)

func main() {
	workerPoolCount := flag.Int("worker-pool", 4, "Worker pool count (Required)")
	needHelp := flag.String("help", "false", "Print all commands")
	flag.Parse()

	if *needHelp == "true" || *needHelp == "1" || *needHelp == "y" || *needHelp == "Y" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	duration := 5 * time.Second

	s := ims.NewInMemoryScheduler(*workerPoolCount)
	task, err := ims.NewTask(
		"Payload multiplication task",
		time.Now().Add(3 * time.Second),
		[]interface{}{1,2,3,4,5},
		&duration,
	)
	if err != nil {
		fmt.Println(fmt.Errorf("can't create task 1: %w", err))
	}

	s.AddTask(*task)

	// task, err = ims.NewTask(
	// 	"Payload concatenation task",
	// 	time.Now().Add(10 * time.Second),
	// 	[]interface{}{"Hello", ", ", "world!"},
	// 	&duration,
	// )
	// if err != nil {
	// 	fmt.Println(fmt.Errorf("can't create task 1: %w", err))
	// }

	// s.AddTask(*task)

	s.RegisterHandler("Payload multiplication task", func(payload []interface{}) error {
		res := 1
		for _, i := range payload {
			val, ok := i.(int)
			if !ok {
				fmt.Println("Task with integers: error: value is not int")
			}

			res = res * val
		}

		if res > 1 {
			fmt.Printf("Payload multiplication task: %d", res)
		}

		return nil
	})

	time.Sleep(20 * time.Second)
	
	s.Close()
}
