CREATE TABLE
    "customers" (
        "id" BIGINT PRIMARY KEY,
        "name" VARCHAR(255) NOT NULL,
        "email" VARCHAR(255) NOT NULL,
        "addresses" JSONB NOT NULL,
        "password" VARCHAR(25),
        "created_at" TIMESTAMP,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );

COMMENT ON COLUMN customers.addresses IS 'Адреса доставки пользователя в формате: ["Краснодар, ул.Российская 74/3 кв.10", ...]';

-- Ищем покупателей по email
CREATE INDEX customers_email_idx ON customers (email);

-- Ищем покупателей по имени
CREATE INDEX customers_name_idx ON customers (NAME);

CREATE INDEX customers_addresses_jsonb ON customers USING GIN (addresses);

CREATE TABLE
    "customers_credentials" (
        "customer_id" BIGINT PRIMARY KEY,
        "password" VARCHAR(25),
        "salt" VARCHAR(10),
        "updated_at" TIMESTAMP
    );
   
ALTER TABLE "customers_credentials" ADD CONSTRAINT "customers_credentials_customer_id" FOREIGN KEY ("customer_id") REFERENCES "customers" ("id");

CREATE INDEX customers_credentials_customer_idx ON customers_credentials (customer_id);

CREATE TABLE
    "categories" (
        "id" BIGINT PRIMARY KEY,
        "name" TEXT NOT NULL,
        "parent_id" BIGINT,
        "is_actual" bool,
        "created_at" TIMESTAMP,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );

ALTER TABLE "categories" ADD CONSTRAINT "parent_category" FOREIGN KEY ("parent_id") REFERENCES "categories" ("id");

-- Ищем категории по name
CREATE INDEX categories_name_idx ON categories (NAME);

CREATE TABLE
    "orders" (
        "id" BIGINT PRIMARY KEY,
        "customer_id" BIGINT NOT NULL,
        "order_date" TIMESTAMP NOT NULL,
        "status_id" BIGINT NOT NULL,
        "products" JSONB NOT NULL,
        "price" FLOAT NOT NULL,
        "discont" FLOAT
    );

COMMENT ON COLUMN orders.products IS 'Товары в заказе в формате: [{"product_id": 1, "count": 10}]';
COMMENT ON COLUMN orders.discont IS 'Суммарная скидка на все товары в заказе в процентах';

ALTER TABLE "orders" ADD CONSTRAINT "orders_customer" FOREIGN KEY ("customer_id") REFERENCES "customers" ("id");

-- Ищем заказы по order_date
CREATE INDEX orders_date_idx ON orders (order_date);

-- Ищем заказы по покупателю
CREATE INDEX orders_customer_idx ON orders (customer_id);

CREATE INDEX orders_products_jsonb ON orders USING GIN (products);

CREATE TABLE
    "order_statuses" (
        "id" BIGINT PRIMARY KEY,
        "name" VARCHAR(20),
        "created_at" TIMESTAMP,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );
   
-- Ищем статусы заказов по наименованию
CREATE INDEX order_statuses_name_idx ON order_statuses (NAME);

CREATE TABLE
    "products" (
        "id" BIGINT PRIMARY KEY,
        "name" VARCHAR(100) NOT NULL,
        "description" VARCHAR(500) NOT NULL,
        "category_id" BIGINT NOT NULL,
        "is_active" BOOLEAN NOT NULL,
        "shelf_life_count" INT NOT NULL,
        "storage_conditions" VARCHAR(255),
        "ingredients" VARCHAR(500),
        "microelements" VARCHAR(255),
        "price" FLOAT NOT NULL,
        "weight" FLOAT,
        "volume" FLOAT,
        "vendor_id" BIGINT NOT NULL,
        "storage_count" INT NOT NULL,
        "created_at" TIMESTAMP NOT NULL,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );

COMMENT ON COLUMN products.category_id IS 'Категория товара';
COMMENT ON COLUMN products.shelf_life_count IS 'Срок годности товара в днях';
COMMENT ON COLUMN products.storage_conditions IS 'Условия хранения';
COMMENT ON COLUMN products.ingredients IS 'Состав';
COMMENT ON COLUMN products.microelements IS 'Состав микроэлементов в 100г. продукта';
COMMENT ON COLUMN products.storage_count IS 'Количество товара на складе';

-- Ищем товары по категории
CREATE INDEX products_category_idx ON products (category_id);

-- Ищем товары по наименованию
CREATE INDEX products_name_idx ON products (NAME);

ALTER TABLE "products" ADD CONSTRAINT "products_category" FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

CREATE TABLE
    "vendors" (
        "id" BIGINT PRIMARY KEY,
        "name" VARCHAR(100) NOT NULL,
        "created_at" TIMESTAMP NOT NULL,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );
    -- Ищем поставщиков по наименованию
CREATE INDEX vendors_name_idx ON vendors (NAME);

CREATE TABLE
    "promotions" (
        "id" BIGINT PRIMARY KEY,
        "date_from" DATE NOT NULL,
        "date_to" DATE NOT NULL,
        "category_id" BIGINT NOT NULL,
        "product_id" BIGINT,
        "discont" INT NOT NULL,
        "promocode" VARCHAR(10),
        "created_at" TIMESTAMP NOT NULL,
        "updated_at" TIMESTAMP
    );
   
ALTER TABLE "promotions" ADD CONSTRAINT "promotions_category" FOREIGN KEY ("category_id") REFERENCES "categories" ("id");

ALTER TABLE "promotions" ADD CONSTRAINT "promotions_product" FOREIGN KEY ("product_id") REFERENCES "products" ("id");

CREATE INDEX promotions_category_idx ON promotions (category_id);

CREATE INDEX promotions_product_idx ON promotions (product_id);

CREATE TABLE
    delivery_cities (
        "id" BIGINT PRIMARY KEY,
        "name" VARCHAR(255) NOT NULL,
        "created_at" TIMESTAMP NOT NULL,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );
   
   
CREATE TABLE
    couriers (
        "id" BIGINT PRIMARY KEY,
        "name" VARCHAR(50),
        "rating" INT,
        "finished_orders_count" INT,
        "summary_fee" INT,
        "delivery_city_id" BIGINT,
        "created_at" TIMESTAMP NOT NULL,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );
   
ALTER TABLE "couriers" ADD CONSTRAINT "couriers_delivery_city" FOREIGN KEY ("delivery_city_id") REFERENCES "delivery_cities" ("id");

CREATE INDEX couriers_name ON couriers (name);

CREATE TABLE
    delivery (
        "id" BIGINT PRIMARY KEY,
        "delivery_finished_at" TIMESTAMP NOT NULL,
        "courier_id" BIGINT NOT NULL,
        "order_id" BIGINT NOT NULL,
        "delivery_city_id" BIGINT,
        "address" VARCHAR(300) NOT NULL,
        "courier_fee" INT NOT NULL,
        "created_at" TIMESTAMP NOT NULL,
        "updated_at" TIMESTAMP,
        "deleted_at" TIMESTAMP
    );
   
ALTER TABLE "delivery" ADD CONSTRAINT "delivery_courier" FOREIGN KEY ("courier_id") REFERENCES "couriers" ("id");

ALTER TABLE "delivery" ADD CONSTRAINT "delivery_order" FOREIGN KEY ("order_id") REFERENCES "orders" ("id");

CREATE INDEX delivery_courier_idx ON delivery (courier_id);

CREATE INDEX delivery_order_idx ON delivery (order_id);

CREATE INDEX delivery_address_idx ON delivery (address);
